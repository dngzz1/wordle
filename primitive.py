import random


def primitive_game():
    words = import_words("txt/5-letter-words.txt")
    guess = ''
    secret_word = random.choice(words).upper()
    print(secret_word)
    while guess.upper() != secret_word.upper():
        guess = input("Enter a 5 letter word:")
        guess.upper()
        if guess.lower() in words:
            print(get_score(guess, secret_word))
        else:
            print(guess + " is not a valid 5-letter English word.")


def get_score(guess, secret_word):
    guess = guess.upper()
    secret_word = secret_word.upper()
    score_list = []
    i = 0
    for char in guess:
        if char == secret_word[i]:
            score_list.append(2)
        elif char in secret_word:
            score_list.append(1)
        else:
            score_list.append(0)
        i += 1
    return score_list


def import_words(filename):
    my_file = open(filename)
    content = my_file.read()
    content_list = content.split("\n")
    my_file.close()
    return content_list


if __name__ == '__main__':
    primitive_game()
