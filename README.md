# WORDLE
The five-letter word database comes from
https://github.com/charlesreid1/five-letter-words/blob/master/sgb-words.txt

Guess a 5-letter English word. If the position of the letter is correct, it becomes green. If the letter is in the word but the position is not correct, it becomes yellow.

You have 6 tries.
