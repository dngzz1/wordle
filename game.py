import pygame
import random
from sys import exit
from primitive import get_score
from primitive import import_words

pygame.init()
pygame.display.set_caption('Wordle')


# Game settings
class Game:
    is_active = True
    SIZE_OF_TILE = 75
    LEN_OF_WORD = 5
    MAX_GUESSES = 6
    dictionary = import_words("txt/5-letter-words.txt")
    secret_word = random.choice(dictionary).upper()
    mode = 'type'  # two modes: 'check' or 'type'
    clock = pygame.time.Clock()

    # VISUALS
    screen = pygame.display.set_mode((LEN_OF_WORD * SIZE_OF_TILE, (MAX_GUESSES + 1) * SIZE_OF_TILE))
    background_color = (0, 0, 0)
    message_font = pygame.font.Font("font/Pixeltype.ttf", int(SIZE_OF_TILE / 4))
    message_color = (255, 255, 255)
    message = message_font.render('', True, message_color)
    message_pos = (int(LEN_OF_WORD * SIZE_OF_TILE / 2), int((MAX_GUESSES + 0.5) * SIZE_OF_TILE))
    message_rect = message.get_rect(center=message_pos)
    message_timer = pygame.USEREVENT + 1
    message_time_in_milliseconds = 2000

    @staticmethod
    def blit_message(string):
        Game.message = Game.message_font.render(string, True,Game.message_color)
        Game.message_rect = Game.message.get_rect(center=Game.message_pos)
        pygame.time.set_timer(Game.message_timer, Game.message_time_in_milliseconds)

    @staticmethod
    def generate_secret_word():
        return random.choice(Game.dictionary).upper()

    @staticmethod
    def restart():
        Tile.tile_list = []
        Tile.tile_group.empty()
        Game.is_active = True
        Game.blit_message('')
        Game.secret_word = Game.generate_secret_word()


class Tile(pygame.sprite.Sprite):
    tile_list = []
    tile_group = pygame.sprite.Group()

    def __init__(self, char):
        super().__init__()
        self._id = len(Tile.tile_list)
        self.char = char
        self.deletable = True
        self.font = pygame.font.Font('font/Pixeltype.ttf', Game.SIZE_OF_TILE)
        self.image = self.font.render(self.char, True, (120, 120, 120))
        self.rect = self.image.get_rect(topleft=self.get_position(self._id))

    @staticmethod
    def get_position(n):
        x_pos = (n % Game.LEN_OF_WORD) * Game.SIZE_OF_TILE
        y_pos = int(n / Game.LEN_OF_WORD) * Game.SIZE_OF_TILE

        # offset for borders
        x_pos += int(Game.SIZE_OF_TILE / 3)
        y_pos += int(Game.SIZE_OF_TILE / 3)
        return x_pos, y_pos

    @staticmethod
    def add_tile(char):
        Tile.tile_list.append(Tile(char))
        Tile.tile_group.empty()
        Tile.tile_group.add(Tile.tile_list)

    @staticmethod
    def change_to_green(tile):
        tile.image = tile.font.render(tile.char, True, (0, 255, 0))
        Tile.tile_group.empty()
        Tile.tile_group.add(Tile.tile_list)
        tile.deletable = False

    @staticmethod
    def change_to_red(tile):
        tile.image = tile.font.render(tile.char, True, (255, 0, 0))
        Tile.tile_group.empty()
        Tile.tile_group.add(Tile.tile_list)
        tile.deletable = False

    @staticmethod
    def change_to_yellow(tile):
        tile.image = tile.font.render(tile.char, True, (255, 255, 0))
        Tile.tile_group.empty()
        Tile.tile_group.add(Tile.tile_list)
        tile.deletable = False

    @staticmethod
    def get_last_n_chars(n):
        my_guess = ""
        for i in range(0, n):
            my_guess += Tile.tile_list[-n + i].char
        return my_guess

    @staticmethod
    def remove_last_tile():
        Tile.tile_list.pop()
        Tile.tile_group.empty()
        Tile.tile_group.add(Tile.tile_list)


def run_game():
    Game()
    while True:
        if Game.is_active:
            for event in pygame.event.get():
                # press <ESC> or red cross to quit game.
                if event.type == pygame.QUIT or \
                        (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    pygame.quit()
                    exit()
                # intro message
                if len(Tile.tile_list) == 0:
                    Game.blit_message('Type a word')
                if event.type == pygame.KEYDOWN:
                    if Game.mode == 'check':
                        # press enter to check word
                        if event.key == pygame.K_RETURN:
                            my_guess = Tile.get_last_n_chars(Game.LEN_OF_WORD)
                            # check if my_guess is a valid word.
                            if my_guess.lower() not in Game.dictionary:
                                Game.blit_message(my_guess + ' is not an English word!')
                                continue
                            # okay it is a valid word, check score.
                            Game.mode = 'type'
                            my_score = get_score(my_guess, Game.secret_word)
                            for i in range(0, Game.LEN_OF_WORD):
                                ith_tile = Tile.tile_list[-Game.LEN_OF_WORD + i]
                                if my_score[i] == 2:
                                    Tile.change_to_green(ith_tile)
                                elif my_score[i] == 1:
                                    Tile.change_to_yellow(ith_tile)
                                else:
                                    Tile.change_to_red(ith_tile)
                            # check score
                            if my_score == Game.LEN_OF_WORD * [2]:
                                # we've won
                                Game.blit_message('You win! Press <SPACE> to restart')
                                Game.is_active = False
                                continue
                            elif len(Tile.tile_list) == Game.LEN_OF_WORD * Game.MAX_GUESSES:
                                # we've lost.
                                Game.blit_message('You lose! Press <SPACE> to restart')
                                Game.is_active = False
                                continue
                            else:
                                Game.blit_message('Type another word')
                        # press backspace to delete letter
                        elif event.key == pygame.K_BACKSPACE and len(Tile.tile_list) != 0 \
                                and Tile.tile_list[-1].deletable:
                            Tile.remove_last_tile()
                            Game.mode = 'type'
                        else:  # if user presses any other key, tell them to press <ENTER>
                            Game.blit_message('Press <ENTER>')
                    else:  # Game.mode == 'type
                        # press backspace to delete letter
                        if event.key == pygame.K_BACKSPACE and len(Tile.tile_list) != 0 \
                                and Tile.tile_list[-1].deletable:
                            Tile.remove_last_tile()

                        # add letter to screen
                        char = pygame.key.name(event.key).upper()
                        if len(char) == 1 and char.isalpha():
                            Tile.add_tile(char)
                            if len(Tile.tile_list) % Game.LEN_OF_WORD == 0 and len(Tile.tile_list) != 0:
                                if Tile.tile_list[-1].deletable:
                                    Game.mode = 'check'
                                    Game.blit_message('Press <ENTER>')

                # make message automatically disappear after some milliseconds.
                if event.type == Game.message_timer:
                    Game.blit_message('')

        else:  # game not active
            for event in pygame.event.get():
                if event.type == pygame.QUIT or \
                        (event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE):
                    pygame.quit()
                    exit()
                if event.type == pygame.KEYDOWN and event.key == pygame.K_SPACE:
                    Game.restart()

        # Update display
        Game.screen.fill(Game.background_color)
        Tile.tile_group.draw(Game.screen)
        Tile.tile_group.update()
        Game.screen.blit(Game.message, Game.message_rect)
        pygame.display.update()
        Game.clock.tick(60)


if __name__ == '__main__':
    run_game()
